import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { Router, Route, Switch } from 'react-router-dom';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(compose(window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f)(createStore));

// import 'bootstrap/dist/css/bootstrap.css';
import './style/core/main.scss';

// Auth
import requireAuth from './components/auth/requireAuth';
import Signin from "./components/auth/signin";
import Signup from "./components/auth/signup";

// Dashboard
import Dashboard from "./components/dashboard/dashboard";

// Error
import PageNotFound from "./components/error/pageNotFound";

// Etc
import history from "./history";
import HeaderLayout from "./components/header/headerLayout";

// Newsletter
import NewNewsletter from "./components/newsletter/newsletterNew";
import EditNewsletter from "./components/newsletter/newsletterEdit";
import NewsletterDetail from "./components/newsletter/newsletterDetail";

// Requests
import NewRequest from "./components/requests/requestsNew";
import ForgotPassword from './components/auth/forgotPassword';

function main() {
  ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
      <Router history={history}>
        <Switch>
          <HeaderLayout>
            { /* The second switch is needed for 404s to function correctly*/ }
            <Switch>
              {/* Authentication */}
              <Route path="/" exact component={Signin}/>
              <Route path="/signin" exact component={Signin}/>
              <Route path="/signup" exact component={Signup}/>
              <Route path="/forgot-password" exact component={ForgotPassword}/>
              
              {/* Dashboard */}
              <Route path="/dashboard" exact component={requireAuth(Dashboard)}/>

              {/* Newsletter */}
              <Route path="/newsletter/new" exact component={requireAuth(NewNewsletter)}/>
              <Route path="/newsletter/edit/:id" exact component={requireAuth(EditNewsletter)}/>
              <Route path="/newsletter/detail/:id" exact component={requireAuth(NewsletterDetail)}/>

              {/* Requests */}
              <Route path="/request/new" exact component={requireAuth(NewRequest)}/>

              {/* 404 Page */}
              <Route component={PageNotFound}/>
            </Switch>
          </HeaderLayout>
        </Switch>
      </Router>
    </Provider>
    , document.querySelector('.app-wrapper'));
}

document.addEventListener('DOMContentLoaded', main);