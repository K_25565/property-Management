import {
    SET_NEWSLETTERS,
    FETCH_NEWSLETTER_ID
} from "../actions/types";

const INITIAL_STATE = {
    newsletters: [],
    newsletterToEdit: {},
}

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_NEWSLETTERS:
            const newsletters = action.payload

            // Sort the newsletters array before returning it to the state
            newsletters.sort((firstNewsletter, secondNewsletter) => firstNewsletter.date < secondNewsletter.date ? 1 : -1);

            return {
                ...state,
                newsletters,
            }

        case FETCH_NEWSLETTER_ID:
            const newsletterID = action.payload;
            var newsletterToEdit = INITIAL_STATE.newsletterToEdit; // this was once an empty object but it didn't work

            state.newsletters.map(newsletter => {
                if(newsletter._id == newsletterID) {
                    newsletterToEdit = newsletter;
                } else {
                }
            })

            return {
                ...state,
                newsletterToEdit,
            };
    
        default:
            //console.log("I defaulted on my loan...");
            return state;
    }
}