import {
    UPDATE_HEADER
} from "../actions/types";

const INITIAL_STATE = {
    title: "",
    subtitle: "",
    hideBar: false,
    showButton: false,
}

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case UPDATE_HEADER:
            const { title, subtitle, hideBar, showButton, } = action.payload;

            return {
                ...state,
                title,
                subtitle,
                hideBar,
                showButton,
            }
            // or we could do
            // return {
            //     ...state,
            //     ...action.payload
            // }

        default: return state;
    }
}