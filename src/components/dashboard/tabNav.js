import React, { Component } from "react";

export default class TabNav extends Component {
    render() {
        return (
            <div className="tab-nav">
                <div className="tab-nav--tabs">
                    {
                        this.props.tabs.map((tab, index) => {
                            const className = `tab-nav--tab ${tab.active ? "tab-nav--active" : null}`
                            return <a key={index} onClick={() => this.props.handleClick(tab.title)} className={className}>{tab.title}</a>
                        })
                    }
                </div>
                {
                    this.props.tabs.map((tab, index) => {
                        if (tab.active) {
                            return (
                                <div key={index} className="tab-nav--component">
                                    {tab.component}
                                </div>
                            )
                        };
                    })
                }
                
            </div>
        );
    };
};