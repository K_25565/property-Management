import React, { Component } from "react";

import { connect } from "react-redux";
import * as actions from "../../actions";

import TextLink from "../form/textLink";

class PageNotFound extends Component {
    componentDidMount() {
        this.props.updateHeader("Error 404", "The requested web page was not found on this server.", false);
    }

    render() {
        return (
            <div className="page-not-found">
                <h1 className="page-not-found--title">Oops!  You seem to be lost.</h1>
                <p className="page-not-found--description">Here are some links to help you out:</p>
                <div className="page-not-found--text-links">
                    <TextLink to="/dashboard" text="Dashboard"/>
                    <TextLink to="/signin" text="Sign In"/>
                </div>
            </div>
            
        );
    }
};

export default connect(null, actions)(PageNotFound);