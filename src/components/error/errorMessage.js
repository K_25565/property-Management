import React from "react";

export default function ErrorMessage({ className, message }) {
    return (
        <div className={`${className} error-message-off`}>
            {message}
        </div>
    );
};