import React, { Component } from "react";

import { connect } from "react-redux";
import * as actions from "../../actions";

import Button from "../misc/button";
import history from "../../history";

export function HeaderContent({ title, subtitle }) {
    return (
        <div className="header-text">
            <h1 className="header-text--title">{title}</h1>
            <p className="header-text--subtext">{subtitle}</p>
        </div>
    );
};

export function HeaderBar({ hideBar }) {
    if (!hideBar) {
        return <div className="header-bar"></div>
    } else {
        return <div></div>
    }
};

export function HeaderButton({ showButton }) {
    class HeaderButtonClass extends Component {
        logOut() {
            this.props.signOut();
            localStorage.removeItem("token");
            history.push("/signin");
        }
    
        render() {
            if (showButton) {
                return <Button className="header-button" icon="fas fa-sign-out-alt" callback={() => this.logOut()}/>
            } else {
                return <div></div>
            }
        }
    }

    const Connection = connect(null, actions)(HeaderButtonClass);

    return <Connection />
}