import React, { Component } from "react";

import { connect } from "react-redux";

import { HeaderContent, HeaderBar, HeaderButton } from "./headerContent";

class HeaderLayout extends Component {
    render() {
        const { title, subtitle, hideBar, showButton } = this.props;

        return (
            <div className="layout-grid">
                <HeaderContent title={title} subtitle={subtitle} />
                <HeaderBar hideBar={hideBar} />
                <HeaderButton showButton={showButton} />
                { this.props.children }
            </div>
        );
    };
};

function mapStateToProps(state) {
    const header = state.header;
    return {
        ...header
    }
}

export default connect(mapStateToProps)(HeaderLayout);