import React, { Component } from "react";

import { connect } from "react-redux";

import RequestsBox from "./requestsBox";

class RequestsBoxes extends Component {
    render() {
        const { pendingCount, progressCount, completeCount } = this.props;

        return (
            <div className="requests-boxes">
                <RequestsBox icon="fas fa-exclamation-triangle" title={`pending`} count={pendingCount}/>
                <RequestsBox icon="fas fa-wrench" title={`in-progress`} count={progressCount}/>
                <RequestsBox icon="fas fa-check-square" title={`complete`} count={completeCount}/>
            </div>
        );
    }
};

function mapStateToProps(state) {
    const { requests } = state.requests;

    var pendingCount = 0;
    var progressCount = 0;
    var completeCount = 0;

    requests.map(request => {
        if (request.status == "complete") {
            completeCount += 1;
        } else if (request.status == "in-progress") {
            progressCount += 1;
        } else {
            pendingCount += 1;
        }
    })
    
    return {
        pendingCount,
        progressCount,
        completeCount,
    }
}

export default connect(mapStateToProps)(RequestsBoxes);