import React, { Component } from "react";

import AnimateHeight from 'react-animate-height';
import { connect } from "react-redux";
import * as actions from "../../actions";

import Icon from "../misc/icon";
import Button from "../misc/button";
import RequireAdmin from "../auth/requreAdmin";

const ROOT_URL = process.env.SERVER_URL;

class RequestsItem extends Component {
    constructor() {
        super();

        this.state = {
            height: 0,
        };
    };

    toggleDropdown = () => {
        var element = document.getElementById(`requests-item-${this.props._id}`);

        if(this.state.height === 0) {
            element.classList.add("bg-F0");
            this.setState({ height: "auto" });
        } else {
            element.classList.remove("bg-F0");
            this.setState({ height: 0 });
        };
    };

    handleStatus = () => {
        const  { _id, status } = this.props;

        this.props.changeStatus({ _id, status }, () => {
            this.props.fetchRequests();
        });
    }

    render() {
        const { title, body, date, imageUrl, status, _id, posterName, posterUnit } = this.props;
        const parsedDate = new Date(date);

        var mainIcon = "fas fa-exclamation-triangle"
        var moveButtonIcon = "fas fa-wrench";

        // Logic for the moveButtonIcon
        switch (status) {
            case "pending":
                moveButtonIcon = "fas fa-wrench";
                break;

            case "in-progress":
                moveButtonIcon = "fas fa-check-square";
                break;

            case "complete":
                moveButtonIcon = "fas fa-exclamation-triangle";
                break;

            default: break;
        }

        // Logic for the mainIcon
        switch (status) {
            case "pending":
                mainIcon = "fas fa-exclamation-triangle";
                break;

            case "in-progress":
                mainIcon = "fas fa-wrench";
                break;

            case "complete":
                mainIcon = "fas fa-check-square";
                break;

            default: break;
        }

        return (
            <div id={`requests-item-${_id}`} className="requests-item">
                <Icon className="requests-item--icon" icon={mainIcon} /> 

                <div className="requests-item--title">
                    <div className="requests-item--title--text">
                        {title}
                    </div>
                    <Icon callback={() => this.toggleDropdown()} className="requests-item--title--arrow" icon="fas fa-sort-down" />
                </div>

                <div className="requests-item--tenant-unit">
                    {posterName ? posterName : "Unknown Poster"} - Unit {posterUnit ? posterUnit : "Unknown"}
                </div>

                <div className={!this.props.admin ? "requests-item--date" : "requests-item--date requests-item--moving-date"}>
                    {parsedDate.getMonth() + 1 }
                    /
                    {parsedDate.getDate()}
                    /
                    {parsedDate.getFullYear() - 2000}
                    {/*Change this -2000 to -3000 at the dawn of the new millenium.*/}
                </div>

                <RequireAdmin>
                    <Button
                        className="requests-item--move"
                        icon={moveButtonIcon}
                        callback={ () => this.handleStatus()}
                    />
                </RequireAdmin>

                <div className="requests-item--description">
                    <AnimateHeight
                        duration={300}
                        height={this.state.height}
                    >
                        <div className="item-description">
                            <img 
                                className="item-description--image"
                                src={`${ROOT_URL}/${imageUrl}`}
                                alt="Request Image"
                            />
                            <p className="item-description--text">
                                {body}
                            </p>
                        </div>
                    </AnimateHeight>
                </div>    
            </div>
        );
    }
};

function mapStateToProps(state) {
    const { admin } = state.auth.user;
    return { admin };
}

export default connect(mapStateToProps, actions)(RequestsItem);