import React, { Component } from "react";

import { connect } from "react-redux";
import * as actions from "../../actions";

import Button from "../misc/button"
import RequestsBoxes from "./requestsBoxes";
import Requests from "./requests";

class RequestsGrid extends Component {
    handleAddRequests = () => {
        this.props.history.push("/request/new");
    }

    componentDidMount() {
        this.props.fetchRequests();
    }

    render() {
        return (
            <div className="requests-grid">
                <Button className="requests-grid--button" icon="fas fa-plus" callback={() => this.handleAddRequests()}/>
                <RequestsBoxes />
                <Requests />
            </div>
        );
    }
};

export default connect(null, actions)(RequestsGrid);