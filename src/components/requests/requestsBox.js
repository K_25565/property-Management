import React, { Component } from "react";

import { connect } from "react-redux";
import * as actions from "../../actions";

class RequestsBox extends Component {
    render() {
        const { count, title, icon } = this.props
        const className = `requests-box ${this.props.selectedRequestType == title ? "requests-box-active" : "requests-box-inactive"}`;

        return (
            <a onClick={() => this.props.changeSelectedRequestType(title)} className={className}>
                <div className="requests-box--count">{count}</div>
                <div className="requests-box--title">{title}</div>
                <div className="requests-box--point"></div>
                <i className={`${icon} requests-box--icon`}></i>
            </a>
        )
    }
}

function mapStateToProps(state) {
    const { selectedRequestType } = state.requests;

    return {
        selectedRequestType
    };
}

export default connect(mapStateToProps, actions)(RequestsBox);