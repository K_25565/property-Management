import React, { Component } from "react";

import {connect} from "react-redux";
import * as actions from "../../actions";

import NewNewsletterForm from "../newsletter/newsletterNewForm";

class NewRequest extends Component {
    onSubmit = (fields) => {
        const { title, body, image } = fields;

        if (!title)
        {
            alert("Please enter a newsletter title.");
        }
        else if (!body)
        {
            alert("Please enter the contents of your newsletter.");
        }
        else if (!image)
        {
            // Temporary fix
            alert("Please select an image.");
        }
        else
        {
            var formData = new FormData();
            formData.append("title", title);
            formData.append("body", body);;
            formData.append("image", image);
            formData.append("postedBy", this.props._id);
    
            this.props.createNewRequest(formData, () => {
                this.props.history.push("../dashboard");
            });
        }
    }

    onCancel = () => {
        this.props.history.push("../dashboard");
    }

    render() {
        return (
            <div className="new-request">
                <NewNewsletterForm 
                    onCancel={() => this.onCancel()} 
                    onSubmit={(event) => this.onSubmit(event)}
                    title="New Request"
                    fieldOnePlaceholder="Title goes here"
                    fieldOneTitle="Service Request Title: "
                    fieldTwoPlaceholder="Description goes here"
                    fieldTwoTitle="Service Request Description: "
                />
            </div>
        );
    }
};

function mapStateToProps(state) {
    const { _id } = state.auth.user;
    return { _id };
}

export default connect(mapStateToProps, actions)(NewRequest);