import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions";

import SignupForm from "./signupForm";

class Signup extends Component {
    componentDidMount() {
        this.props.updateHeader("Welcome to HOA Manager!", "Please login to continue", false);
    }

    onSubmit = (fields) => {
        const { fullname, unit, email, password } = fields;

        if (!fullname)
        {
            alert("Please enter your name.");
        }
        else if (!unit)
        {
            alert("Please enter a your unit information.");
        }
        else if (!email)
        {
            alert("Please enter a valid email.");
        }
        else if (!password)
        {
            alert("Please enter a password.")
        }
        else
        {
            // Change the boolean below if you want accounts to be admin accounts
            this.props.signUp(false, fields, () => {
                console.log("navigate to dashboard");
                this.props.history.push("/dashboard");
            });
        }      
    }

    render() {
        return (
            <div className="sign-up">
                <SignupForm onSubmit={(event) => this.onSubmit(event)}/>
            </div>
        );
    }
};

export default connect(null, actions)(Signup);