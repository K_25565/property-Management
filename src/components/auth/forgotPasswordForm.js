import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";

import FormTitle from "../form/formTitle";
import { FormInput, FormButton } from "../form/formFields";
import TextLink from "../form/textLink";

class ForgotPasswordForm extends Component {
    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={ handleSubmit } className="forgot-password-form">
                <FormTitle className="forgot-password-form--title" text="Forgot Password"/>
                <Field 
                    className="forgot-password-form--email"
                    placeholder="Enter Email"
                    title="Email: "
                    name="email"
                    type="email"
                    component={FormInput}
                />
                <Field 
                    className="forgot-password-form--new-password"
                    placeholder="Enter New Password"
                    title="New Password: "
                    name="newPassword"
                    type="password"
                    component={FormInput}
                />
                <Field 
                    className="forgot-password-form--new-password-confirm"
                    placeholder="Enter New Password"
                    title="Confirm New Password: "
                    name="newPasswordConfirm"
                    type="password"
                    component={FormInput}
                />
                <Field 
                    className="forgot-password-form--change-password"
                    small={false}
                    danger={false}
                    title="Change Password"
                    name="changePassword"
                    type="submit"
                    component={FormButton}
                />
                <div className="forgot-password-form--text-link">
                    <TextLink to="/signin" text="Remember your password?  Login here"/>
                </div>
            </form>
        );
    }
};

export default ForgotPasswordForm = reduxForm({
    form: "forgotpassword"
})(ForgotPasswordForm);