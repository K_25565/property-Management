import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";

import FormTitle from "../form/formTitle";
import { FormInput, FormButton } from "../form/formFields";
import TextLink from "../form/textLink";

class SignupForm extends Component {
    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit} className="sign-up-form">
                <FormTitle className="sign-up-form--title" text="New User"/>
                <Field 
                    className="sign-up-form--fullname"
                    placeholder="Enter Your Full Name"
                    title="Full Name: "
                    name="fullname"
                    type="text"
                    component={FormInput}
                />
                <Field 
                    className="sign-up-form--unit"
                    placeholder="Enter Unit #"
                    title="Unit #: "
                    name="unit"
                    type="text"
                    component={FormInput}
                />
                <Field 
                    className="sign-up-form--email"
                    placeholder="Enter Email"
                    title="Email: "
                    name="email"
                    type="email"
                    component={FormInput}
                />
                <Field 
                    className="sign-up-form--password"
                    placeholder="Enter Password"
                    title="Password: "
                    name="password"
                    type="password"
                    component={FormInput}
                />
                <Field 
                    className="sign-up-form--create-account"
                    small={false}
                    danger={false}
                    title="Create Account"
                    name="createaccount"
                    type="submit"
                    component={FormButton}
                />
                <div className="sign-up-form--text-links">
                    <TextLink to="/signin" text="Already registered?  Login."/>
                </div>
            </form>
        );
    }
};

export default SignupForm = reduxForm({
    form: "signup"
})(SignupForm);