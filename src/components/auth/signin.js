import React, { Component } from "react";

import { connect } from "react-redux";
import * as actions from "../../actions";

import SignInForm from "./signinForm";

class Signin extends Component {
    componentDidMount() {
        this.props.updateHeader("Welcome to HOA Manager!", "Please login to continue", false);
    }

    onSubmit = (fields) => {
        const { email, password} = fields;

        if (!email)
        {
            alert("Please type a valid email address.")
        }
        else if (!password)
        {
            alert("Please type a password.")
        }
        else
        {
            this.props.signIn(fields, () => {
                this.props.history.push("/dashboard");
            });
        }
    }

    render() {
        return (
            <div className="sign-in">
                <SignInForm onSubmit={(event) => this.onSubmit(event)}/>
            </div>
        );
    }
};

export default connect(null, actions)(Signin);