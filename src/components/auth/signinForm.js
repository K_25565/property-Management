import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";

import FormTitle from "../form/formTitle";
import { FormInput, FormButton } from "../form/formFields";
import TextLink from "../form/textLink";

class SigninForm extends Component {
    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={ handleSubmit } className="sign-in-form">
                <FormTitle className="sign-in-form--title" text="Login"/>
                <Field 
                    className="sign-in-form--email"
                    placeholder="Enter Email"
                    title="Email: "
                    name="email"
                    type="email"
                    component={FormInput}
                />
                <Field 
                    className="sign-in-form--password"
                    placeholder="Enter Password"
                    title="Password: "
                    name="password"
                    type="password"
                    component={FormInput}
                />
                <Field 
                    className="sign-in-form--login"
                    small={false}
                    danger={false}
                    title="Login"
                    name="login"
                    type="submit"
                    component={FormButton}
                />
                <div className="sign-in-form--text-links">
                    <TextLink to="/forgot-password" text="Forgot password"/>
                    <TextLink to="/signup" text="Not a member?  Register here"/>
                </div>
            </form>
        );
    }
};

export default SigninForm = reduxForm({
    form: "signin"
})(SigninForm);