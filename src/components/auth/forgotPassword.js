import React, { Component } from "react";

import { connect } from "react-redux";
import * as actions from "../../actions";

import ForgotPasswordForm from "./forgotPasswordForm";

class ForgotPassword extends Component {
    onSubmit = (fields) => {
        const { email, newPassword, newPasswordConfirm } = fields;

        if (!email)
        {
            alert("Please enter a valid email.");
        }
        else if (!newPassword)
        {
            alert("Please enter a new password.")
        }
        else if (!newPasswordConfirm)
        {
            alert("Please enter your new password again.")
        }
        else if (newPassword != newPasswordConfirm)
        {
            alert("Your passwords do not match!");
        }
        else
        {
            this.props.resetPassword(fields, () => {
                this.props.history.push("/signin");
            })
        }
    }

    render() {
        return (
            <div className="forgot-password">
                <ForgotPasswordForm onSubmit={(event) => this.onSubmit(event)}/>
            </div>
        );
    };
};

export default connect(null, actions)(ForgotPassword);