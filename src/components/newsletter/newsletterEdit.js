import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions";

import EditNewsletterForm from "./newsletterEditForm";

class EditNewsletter extends Component {
    onSubmit = (fields) => {
        const { title, body, image } = fields;

        if (!title)
        {
            alert("Please enter a newsletter title.");
        }
        else if (!body)
        {
            alert("Please enter the contents of your newsletter.");
        }
        /*
        else if (!image)
        {
            // Temporary fix
            alert("Please select an image.");
        }
        */
        else
        {
            var formData = new FormData();
            formData.append("_id", this.props.match.params.id);
            formData.append("title", title);
            formData.append("body", body);   
            formData.append("image", image);
    
            this.props.editNewsletter(formData, () => {
                this.props.history.push("../../dashboard");
            });
        }
    };

    onCancel = () => {
        this.props.history.push("../../dashboard");
    };
    
    componentDidMount() {
        this.props.fetchNewsletterById(this.props.match.params.id);
    };

    render() {
        return (
            <div className="new-newsletter">
                <EditNewsletterForm 
                    onCancel={() => this.onCancel()}
                    onSubmit={(event) => this.onSubmit(event)}
                    title="Edit Newsletter"
                    fieldOnePlaceholder="Title goes here"
                    fieldOneTitle="Newsletter Title: "
                    fieldTwoPlaceholder="Body goes here"
                    fieldTwoTitle="Newsletter Body: "
                />
            </div>
        );
    };
};

export default connect(null, actions)(EditNewsletter);