import React, { Component } from "react";

import Button from "../misc/button";

import history from "../../history";
import RequireAdmin from "../auth/requreAdmin";

const ROOT_URL = process.env.SERVER_URL;

export default class NewsletterLatest extends Component {
    handleEdit = () => {
        history.push(`/newsletter/edit/${this.props._id}`);
    };

    render() {
        const { title, imageUrl, body } = this.props;

        return (
            <div className="newsletter-latest">
                <h1 className="newsletter-latest--title">{title}</h1>
                <img className="newsletter-latest--image" src={`${ROOT_URL}/${imageUrl}`} alt="Newsletter Image"/>

                <RequireAdmin>
                    <Button className="newsletter-latest--button" callback={() => this.handleEdit()} icon="fas fa-edit" />
                </RequireAdmin>
                <div className="newsletter-latest--body">
                    <p>
                        {body}
                    </p>
                </div>
            </div>
        );
    };
};