import React, { Component } from "react";

const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
export default class NewsletterBox extends Component {
    render() {
        const { date } = this.props;

        if (!date) {
            return (
                <div>...fetching newsletter</div>
            )
        }

        const parsedDate = new Date(date);

        return (
            <div className="newsletter-box">
                <div className="newsletter-box--day">{parsedDate.getDate()}</div>
                <div className="newsletter-box--month-year">{`${monthNames[parsedDate.getMonth()]} ${parsedDate.getFullYear()}`}</div>
                <div className="newsletter-box--point"></div>
            </div>
        )
    }
}