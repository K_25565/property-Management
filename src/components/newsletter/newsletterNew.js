import React, { Component } from "react";

import {connect} from "react-redux";
import * as actions from "../../actions";

import NewNewsletterForm from "./newsletterNewForm";

class NewNewsletter extends Component {
    onSubmit = (fields) => {
        const { title, body, image } = fields;

        if (!title)
        {
            alert("Please enter a newsletter title.");
        }
        else if (!body)
        {
            alert("Please enter the contents of your newsletter.");
        }
        else if (!image)
        {
            alert("Please select an image.");
        }
        else
        {
            var formData = new FormData();
            formData.append("title", title);
            formData.append("body", body);
            formData.append("image", image);   

            console.log(formData);
    
            this.props.createNewNewsletter(formData, () => {
                this.props.history.push("../dashboard");
            });
        }
    }

    onCancel = () => {
        this.props.history.push("../dashboard");
    }

    render() {
        return (
            <div className="new-newsletter">
                <NewNewsletterForm 
                    onCancel={() => this.onCancel()} 
                    onSubmit={(event) => this.onSubmit(event)}
                    title="New Newsletter"
                    fieldOnePlaceholder="Title goes here"
                    fieldOneTitle="Newsletter Title: "
                    fieldTwoPlaceholder="Body goes here"
                    fieldTwoTitle="Newsletter Body: "
                />
            </div>
        );
    }
};

export default connect(null, actions)(NewNewsletter);