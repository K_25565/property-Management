import React, { Component } from "react";

import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";

import FormTitle from "../form/formTitle";
import { FormInput, FormButton, FormTextArea, FormImage } from "../form/formFields";

const ROOT_URL = process.env.SERVER_URL;

class EditNewsletterForm extends Component {
    render() {
        const { handleSubmit, formTitle } = this.props;
        const { 
            fieldOnePlaceholder, 
            fieldOneTitle,
            fieldTwoPlaceholder,
            fieldTwoTitle,
        } = this.props;

        return (
            <form onSubmit={ handleSubmit } className="new-newsletter-form">
                <FormTitle className="new-newsletter-form--title" text={formTitle}/>
                <Field 
                    className="new-newsletter-form--newsletter-title"
                    placeholder={fieldOnePlaceholder}
                    title={fieldOneTitle}
                    name={"title"}
                    type="text"
                    component={FormInput}
                />
                <Field 
                    className="new-newsletter-form--body"
                    placeholder={fieldTwoPlaceholder}
                    title={fieldTwoTitle}
                    name="body"
                    type="text"
                    component={FormTextArea}
                />
                <Field 
                    className="new-newsletter-form--submit"
                    small={true}
                    danger={true}
                    title="Submit"
                    name="submit"
                    type="submit"
                    component={FormButton}
                    //onClick={this.props.onSubmit} // Commented out as this would call onSubmit twice.
                />
                <Field 
                    className="new-newsletter-form--cancel"
                    small={true}
                    danger={false}
                    title="Cancel"
                    name="cancel"
                    type="button"
                    component={FormButton}
                    onClick={this.props.onCancel}
                />
                <Field 
                    className="new-newsletter-form--image"
                    title="Image"
                    name="image"
                    type="file"
                    component={FormImage}
                    imageUrl={this.props.initialValues.imageUrl ? `${ROOT_URL}/${this.props.initialValues.imageUrl}` : "http://via.placeholder.com/150x137"}
                />
            </form>
        );
    }
};

EditNewsletterForm = reduxForm({
    form: "editnewsletter",
    enableReinitialize: true
})(EditNewsletterForm);

function mapStateToProps(state) {
    const { newsletterToEdit } = state.newsletters;
    return {
        initialValues: newsletterToEdit
    }
}

export default connect(mapStateToProps)(EditNewsletterForm)