import axios from "axios";
import {
    SET_NEWSLETTERS,
    FETCH_NEWSLETTER_ID
} from "./types";

const ROOT_URL = process.env.SERVER_URL;

export function fetchNewsletters() {
    const token = localStorage.getItem("token");
    const headers = { headers: {authorization: token}};

    return function(dispatch) {
        axios.get(`${ROOT_URL}/newsletters`, headers)
        .then(response => {
            dispatch({
                type: SET_NEWSLETTERS,
                payload: response.data
            })
        })
        .catch(err => {
            console.error(err);
        })
    } 
};

export function fetchNewsletterById(id) {
    return {
        type: FETCH_NEWSLETTER_ID,
        payload: id
    }
}

export function createNewNewsletter(formData, success) {
    const token = localStorage.getItem("token");
    const headers = { headers: {
        "Content-Type": "multipart/form-data",
        authorization: token
    }}

    return function() {
        axios.post(`${ROOT_URL}/newsletter/new`, formData, headers)
        .then(response => {
            console.log(response.data);
            success();
        })
        .catch(err => {
            console.error(err);
        });
    };
};

export function editNewsletter(formData, success) {
    const token = localStorage.getItem("token");
    const headers = { headers: {
        "Content-Type": "multipart/form-data",
        authorization: token
    }}
    
    return function() {
        axios.post(`${ROOT_URL}/newsletters/edit/${formData._id}`, formData, headers)
        .then(response => {
            console.log(response.data);
            success();
        })
        .catch(err => {
            console.error(err);
        });
    };
}