// Authentication Actions
export const AUTHENTICATE_USER = "AUTHENTICATE_USER";
export const DEAUTHENTICATE_USER = "DEAUTHENTICATE_USER";

// Newsletter Actions
export const SET_NEWSLETTERS = "SET_NEWSLETTERS";
export const FETCH_NEWSLETTER_ID = "FETCH_NEWSLETTER_ID";

// Requests Actions
export const SET_REQUESTS = "SET_REQUESTS";
export const CHANGE_SELECTED_REQUEST_TYPE = "CHANGE_SELECTED_REQUEST_TYPE";

// Header Actions
export const UPDATE_HEADER = "UPDATE_HEADER";