import axios from "axios";
import { 
    AUTHENTICATE_USER,
    DEAUTHENTICATE_USER,
} from "./types";

const ROOT_URL = process.env.SERVER_URL;

export function signUp(createAdminAccount, fields, success) {
    if (createAdminAccount) {
        var adminFields = {
            admin: true
        }

        adminFields = {
            ...fields,
            ...adminFields,
        }

        //console.log(adminFields);

        return function(dispatch) {
            axios.post(`${ROOT_URL}/signUp`, adminFields)
            .then(response => {
                dispatch({
                    type: AUTHENTICATE_USER,
                    payload: response.data,
                })
    
                const { token } = response.data;
                localStorage.setItem("token", token);
    
                success();
            })
            . catch(error => {
                if(error) { alert(error) }
            });
        }
        
    } else {
        return function(dispatch) {
            axios.post(`${ROOT_URL}/signUp`, fields)
            .then(response => {
                dispatch({
                    type: AUTHENTICATE_USER,
                    payload: response.data,
                })

                const { token } = response.data;
                localStorage.setItem("token", token);

                success();
            })
            . catch(error => {
                if(error) { alert(error) }
            });
        };
    }
};

export function signIn(fields, success) {
    return function(dispatch) {
        axios.post(`${ROOT_URL}/signin`, fields)
        .then(response => {
            const { token } = response.data;
            localStorage.setItem("token", token);

            dispatch({
                type: AUTHENTICATE_USER,
                payload: response.data,
            })
            success();
        })
        .catch(error => {
            if(error) { alert(error) };
        })
    }
}

export function resetPassword(fields, success) {
    return function() {
        axios.post(`${ROOT_URL}/forgot-password`, fields)
        .then (response => {
            //console.log(response.data);
            success();
        })
        .catch(error => {
            if(error) { alert(error) };
        })
    }
}

export function signOut() {
    return {
        type: DEAUTHENTICATE_USER,
        payload: null,
    }
}