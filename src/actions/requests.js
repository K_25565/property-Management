import axios from "axios";
import {
    CHANGE_SELECTED_REQUEST_TYPE,
    SET_REQUESTS,
} from "./types";

const ROOT_URL = process.env.SERVER_URL;

export function changeSelectedRequestType(boxType) {
    return (
        {
            type: CHANGE_SELECTED_REQUEST_TYPE,
            payload: boxType,
        }
    )
};

export function createNewRequest(formData, success) {
    const token = localStorage.getItem("token");
    const headers = { headers: {
        "Content-Type": "multipart/form-data",
        authorization: token
    }}

    return function() {
        axios.post(`${ROOT_URL}/requests/new`, formData, headers)
        .then(response => {
            console.log(response.data);
            success();
        })
        .catch(err => {
            console.error(err);
        });
    };
};

export function fetchRequests() {
    const token = localStorage.getItem("token");
    const headers = { headers: { authorization: token }};

    return function(dispatch) {
        axios.get(`${ROOT_URL}/requests`, headers)
        .then(response => {
            dispatch({
                type: SET_REQUESTS,
                payload: response.data,
            })
        })
        .catch(err => {
            console.error(err);
        });
    }
}

export function changeStatus({_id, status}, success) {
    const token = localStorage.getItem("token");
    const headers = {headers: {authorization: token}};

    return function() {
        axios.post(`${ROOT_URL}/requests/update-status`, { _id, status }, headers)
        .then(response => {
            console.log(response.data);
            success();
        })
        .catch(err => {
            console.error(err);
        })
    }
}