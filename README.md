# Property Management Application

> The template is provided for the students of the [Bottega Code School](https://bottega.tech/) and is a *fork from [es6-webpack2-starter](https://github.com/micooz/es6-webpack2-starter)*.

This project is the client-side of a web application designed to represent a HOA manager.  Currently, users are able to login to their account, create new accounts, and change the password to their account.  Furthermore, users are able to log in and read newsletters as well as submit maintenence requests.

While this project was originally started in 2018 as part of the Bottega Code School, it has been edited in 2023 to fix bugs that were present in the original code.

A live version of this project can be found [here](https://property-management-0ttl.onrender.com).

The server back-end for this project can be found in [this](https://gitlab.com/K_25565/property-management-server) repository.